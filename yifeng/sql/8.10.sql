-- --------------------------------------------------------
-- 主机:                           localhost
-- 服务器版本:                        5.7.25 - MySQL Community Server (GPL)
-- 服务器OS:                        Win64
-- HeidiSQL 版本:                  10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping data for table yifeng.yf_classitem: 10 rows
/*!40000 ALTER TABLE `yf_classitem` DISABLE KEYS */;
REPLACE INTO `yf_classitem` (`id`, `classid`, `name`, `sort`, `value`, `formtype`) VALUES
	(5, 2, '250g', 100, 0.3000, 1),
	(4, 2, '200克', 100, 0.2000, 1),
	(6, 2, '300g', 100, 0.4000, 1),
	(7, 2, '350g', 100, 0.5000, 1),
	(8, 9, '八开', 100, 1.0000, 1),
	(9, 9, '四开', 100, 2.0000, 1),
	(10, 9, '对开', 100, 8.0000, 1),
	(11, 10, '哑膜', 100, 2.0000, 1),
	(12, 10, '亮膜', 100, 3.0000, 1),
	(13, 10, '不覆膜', 100, 0.0000, 1);
/*!40000 ALTER TABLE `yf_classitem` ENABLE KEYS */;

-- Dumping data for table yifeng.yf_classs: 4 rows
/*!40000 ALTER TABLE `yf_classs` DISABLE KEYS */;
REPLACE INTO `yf_classs` (`id`, `name`, `sort`) VALUES
	(11, '刀版费用', 100),
	(2, '纸张克重', 100),
	(9, '印刷费用', 100),
	(10, '覆膜费用', 100);
/*!40000 ALTER TABLE `yf_classs` ENABLE KEYS */;

-- Dumping data for table yifeng.yf_manager: 1 rows
/*!40000 ALTER TABLE `yf_manager` DISABLE KEYS */;
REPLACE INTO `yf_manager` (`id`, `username`, `password`) VALUES
	(1, 'admin', '$2y$10$zX6MFq2NpESTCkwe.AZ8l.mY.psZxIIeY7/Q0sq1RQ4V62c396B4u');
/*!40000 ALTER TABLE `yf_manager` ENABLE KEYS */;

-- Dumping data for table yifeng.yf_migrations: ~4 rows (大约)
/*!40000 ALTER TABLE `yf_migrations` DISABLE KEYS */;
REPLACE INTO `yf_migrations` (`version`, `migration_name`, `start_time`, `end_time`, `breakpoint`) VALUES
	(20190621153738, 'password', '2019-06-24 17:42:18', '2019-06-24 17:42:18', 0),
	(20190626150822, 'Manager', '2019-06-26 23:11:39', '2019-06-26 23:11:39', 0),
	(20190717074336, 'Classitem', '2019-07-17 16:26:17', '2019-07-17 16:26:18', 0),
	(20190717074516, 'Classs', '2019-07-17 16:26:18', '2019-07-17 16:26:18', 0);
/*!40000 ALTER TABLE `yf_migrations` ENABLE KEYS */;

-- Dumping data for table yifeng.yf_users: 101 rows
/*!40000 ALTER TABLE `yf_users` DISABLE KEYS */;
REPLACE INTO `yf_users` (`id`, `username`, `password`, `login_status`, `status`, `login_code`, `last_login_ip`, `last_login_time`, `is_delete`) VALUES
	(1, 'test', '$2y$10$43oV.Gv53YvQx324R3cKGehrPwBEdigvnIcDc0eXTOGa6RWN8yH4u', 0, 0, '0', 0, 0, 0),
	(2, 'test1561369858', '$2y$10$3v9P5s4A4WzuwCHJul3CI.vTXSnio6KLTe0JyG0WfkTTSiScOS9Ga', 0, 0, '0', 0, 0, 0),
	(3, 'test1', '$2y$10$JVOiyfJDL7CjmfubGXMrdun52kEG./Y/xvBI5EwS9vY/m5nqoUZlq', 0, 0, '0', 0, 0, 0),
	(4, 'test2', '$2y$10$RQthO3hytpZEbtJevNtsoOrX2ibdHiQnS8.vTa0hECps49uj83cUC', 0, 0, '0', 0, 0, 0),
	(5, 'test3', '$2y$10$U0gxtAydiBlDgZe.OYOGcOn5qsagZNn3LmjE7MkMHASk/7tI26cUm', 0, 0, '0', 0, 0, 0),
	(6, 'test4', '$2y$10$kfwReYJ0Jvo5A3NKJIbdzuLvHqmWYEFJVJ5OTFDHHXU6b9INjeNQS', 0, 0, '0', 0, 0, 0),
	(7, 'test5', '$2y$10$dD8hNhjNNLRhP/Cybf3w0.j1s2snEASmDe5nsGfqVIznhNAjAD07e', 0, 0, '0', 0, 0, 0),
	(8, 'test6', '$2y$10$0HklhGP4jR5fjVEq.WIh4upvmhzFNfTZqUXpZO0.hFRe1eL/lO7h.', 0, 0, '0', 0, 0, 0),
	(9, 'test7', '$2y$10$C/mV0srS3YPmXiN4iw6o/OsSmWy85lYLdUXPZgkGqJYuPaqZVBnzC', 0, 0, '0', 0, 0, 0),
	(10, 'test8', '$2y$10$98SYoaPUGoFSGliFN8f/BO074ku5kMFsIF28ghlIr/.yCpuFhAn4u', 0, 0, '0', 0, 0, 0),
	(11, 'test9', '$2y$10$4104SMCDSdjNo3m.eAiD8.OSCyw5PmuWCX8h1qsdkTeesoUuTKKEy', 0, 0, '0', 0, 0, 0),
	(12, 'test10', '$2y$10$HByqxls7GFJV6j3f8NIj9eNF.LpIjtvZ61iGOrdLLEwl93vOuvXQW', 0, 0, '0', 0, 0, 0),
	(13, 'test11', '$2y$10$QfLuRZsA3FUouun1x7HBWuN.U4Jeq8G.TFIdPmxmJiwKUcEo6jcmi', 0, 0, '0', 0, 0, 0),
	(14, 'test12', '$2y$10$UaUgbVenagisPBo.QXPyye3fagG8gJgLMji0e8VDzTfetqNryFMU6', 0, 0, '0', 0, 0, 0),
	(15, 'test13', '$2y$10$wwWgxL1mM2TsH4MBQf1cNuNwjv.WPyNF9xbE3ONKFe1LyS2ZFv1PS', 0, 0, '0', 0, 0, 0),
	(16, 'test14', '$2y$10$rX1OFLXNE1QX3DMilKguwu1umgLtarns8A3rF7Gz1U2w.Ws9NH7Si', 0, 0, '0', 0, 0, 0),
	(17, 'test15', '$2y$10$f18q4Bl85esIdK4ZunkSMu3gxl1z5ulfrOp5KlKbZEGBWGefP7UzG', 0, 0, '0', 0, 0, 0),
	(18, 'test16', '$2y$10$bHkubKGyt39CUeWXFoXSnuS1eoqvoS92g0wo0glbAE4vkh89pyFgy', 0, 0, '0', 0, 0, 0),
	(19, 'test17', '$2y$10$9hY99uVmtRjyrqMM4YTPseIctPEq7FoZ06dcE7XdE6Laf4pdfgBEW', 0, 0, '0', 0, 0, 0),
	(20, 'test18', '$2y$10$1Z.gy6MYp0H.MIcwzQ01rupGcW4.3KZXgo6/VhkP5drR0nHSNCsBO', 0, 0, '0', 0, 0, 0),
	(21, 'test19', '$2y$10$hMbIv.qN0R.Mb6C0C5Tmreu5pImIMm2.E5CHmS0YVBH/Awwkhtj0u', 0, 0, '0', 0, 0, 0),
	(22, 'test20', '$2y$10$4tU5HWUi.4a1WH25eQZLdesVWLBuBwoknAFXGu3h0eg5kjBYmjjhW', 0, 0, '0', 0, 0, 0),
	(23, 'test21', '$2y$10$QYLPQUMYp5MmHfacibLRZOWXx/4TMIgjabZJ4xMBHAz2yfqQMayJ2', 0, 0, '0', 0, 0, 0),
	(24, 'test22', '$2y$10$P0XYD2jwwoU0pxrkrgMbsuq.sK0GlspxrKOnLpsOLCBjrxJazH/I2', 0, 0, '0', 0, 0, 0),
	(25, 'test23', '$2y$10$t63InPQ./jp5W1x/NKH9E.bJIgq8KWoETbNbuYos6XgSfWYk5FDQO', 0, 0, '0', 0, 0, 0),
	(26, 'test24', '$2y$10$H0ba9equFj74upQY3I.X3.lJfFqOTmxHSzADlKUYnx1rIOslPrj3m', 0, 0, '0', 0, 0, 0),
	(27, 'test25', '$2y$10$ERLxrRARhsrMzz7DOjKWJO0rQrsgRYdBXXNNNDt6725feC4Uht3tS', 0, 0, '0', 0, 0, 0),
	(28, 'test26', '$2y$10$qRfU7XQ5VDJ249aQpH9XweHoa4gXx1o8YjN0zrYRorRdZiO976l1m', 0, 0, '0', 0, 0, 0),
	(29, 'test27', '$2y$10$bXJvkO/URL7RgeCOZAiXzemb.IN/NVjzTkaZF0kEtAptXsmzh2lhC', 0, 0, '0', 0, 0, 0),
	(30, 'test28', '$2y$10$udf7zVWTQQNalnEWJHo8p.F0omu4fgrQFbUxyp6OAwnlWfKrCZdOW', 0, 0, '0', 0, 0, 0),
	(31, 'test29', '$2y$10$dcHyZKbMyK90egvFe2tp4.qCoPlQivScf121/vUMQVSEXoeS4eV0u', 0, 0, '0', 0, 0, 0),
	(32, 'test30', '$2y$10$cdKK8pMZibc/59TA5aEZCOP7oe0kLBxnk17zqgl7p31cuPhzw8bu2', 0, 0, '0', 0, 0, 0),
	(33, 'test31', '$2y$10$tfxgF4WkvfVKtvw7z3Kmbugl4uN6h2BvnnDx8cQE39.bEwXvdg6cK', 0, 0, '0', 0, 0, 0),
	(34, 'test32', '$2y$10$rs5R3OCye4w4UFQJb8JnTONLw/2XkPP6udLehZdhZMerFUbvmurLO', 0, 0, '0', 0, 0, 0),
	(35, 'test33', '$2y$10$TArAmHQSRqdx8NWsTXCks.U2WrPQ1qkQZI4XWJMROh7e3Ij5uwxfK', 0, 0, '0', 0, 0, 0),
	(36, 'test34', '$2y$10$oiZGmPDrvDFchZ14S806pOwSQlEwOr50czvDqsCPpIemM8rk19D0m', 0, 0, '0', 0, 0, 0),
	(37, 'test35', '$2y$10$w0BN1aJ4OraQyKmbcrzAX.Xqkny00DDQNO6Uh/SKHWjIzil0W9MJ.', 0, 0, '0', 0, 0, 0),
	(38, 'test36', '$2y$10$i.Dc9/N47NXUQuBRqwaH1u.0Li79vJ.eOe4M/4vEv862WK1BZJ9Eq', 0, 0, '0', 0, 0, 0),
	(39, 'test37', '$2y$10$ktqy9CGuX8u0F4/G4xnr/O03/mYQJnPFzFvd1mn8DrgSRYOnc/1OG', 0, 0, '0', 0, 0, 0),
	(40, 'test38', '$2y$10$3YqQQMEBocnPzavmpq2VN.bvfqSY95j4bNhW0BvvN90srTDAJCzfm', 0, 0, '0', 0, 0, 0),
	(41, 'test39', '$2y$10$DQ5KaoSDiYyNwbAwSc4RaO7dcaOD0teoEzh1SCflIdCZut3M72NSO', 0, 0, '0', 0, 0, 0),
	(42, 'test40', '$2y$10$BmhmdLH/OdSxLvPX6l0kI.L2bZAyohTC3tDOCcA.3hPBKfidAdM/u', 0, 0, '0', 0, 0, 0),
	(43, 'test41', '$2y$10$cTrEHkwDMlrvx82zZOQVuuWFD8XDkg9o12h3kTIWIfbBHIzS9mL9C', 0, 0, '0', 0, 0, 0),
	(44, 'test42', '$2y$10$ToFaxAA9HMMoNMNb1RNQUur7nKpw63Pcc.WxfXCuomAaAsNHydg2u', 0, 0, '0', 0, 0, 0),
	(45, 'test43', '$2y$10$b/1tByo7fjVD/76D/VI3OOTpvB4gYjWcxr9aIhxP/EJ2PN5OJyLp6', 0, 0, '0', 0, 0, 0),
	(46, 'test44', '$2y$10$HG72hLzVVTH5.PeYY5IV/uUPjk.KIQdwlsncRcI1cazWBwHag5MkW', 0, 0, '0', 0, 0, 0),
	(47, 'test45', '$2y$10$ziC.77PxcKtjyr3jEeXU8eD7ViTQeq/TxtatB.eZsY5Wai6HHoYIG', 0, 0, '0', 0, 0, 0),
	(48, 'test46', '$2y$10$aLGkb6PYdPtrwifXzRHF3OlsMop3HaBR0OWdFkQFW2RlvO5Spjvv6', 0, 0, '0', 0, 0, 0),
	(49, 'test47', '$2y$10$o2jEPb.BITXo3TVBCYnsquC0hjRDCkmCVMbLkZvrZg47YenQvPdK2', 0, 0, '0', 0, 0, 0),
	(50, 'test48', '$2y$10$iUP3R.vYhKFSf/Z/B.QkT.P9VEB8eaz8U7e9ontU1McoehNzJJyEG', 0, 0, '0', 0, 0, 0),
	(51, 'test49', '$2y$10$uvvfEIqbqecW7OLHOQ9l4.abMOx9o9ho0McYFTYSM0ei9bElFQpd2', 0, 0, '0', 0, 0, 0),
	(52, 'test50', '$2y$10$Jfc3HiKjaxVYQTouWBUv8uItYVfx6kubh727czhIORRN/9do7huTO', 0, 0, '0', 0, 0, 0),
	(53, 'test51', '$2y$10$.Xqm6GybTfCwzxPm4YPpvumw5.CNFjLVTk2vB2m7EmSDGfiERMq2a', 0, 0, '0', 0, 0, 0),
	(54, 'test52', '$2y$10$KzXXpS124jDoYkKTaIAdW.Y.HbnIs/Jfa66zR5e6IE.dzCrySF2RC', 0, 0, '0', 0, 0, 0),
	(55, 'test53', '$2y$10$xVMjzJPZW79YS9H5Va.RROVPWEFDcVsbkzBVTrezgL.SeoQJaYbOa', 0, 0, '0', 0, 0, 0),
	(56, 'test54', '$2y$10$luDDXRqFH05P/eEDbjtHA.F0gaAysOrOcpDRyhY460Drx25Af5U/q', 0, 0, '0', 0, 0, 0),
	(57, 'test55', '$2y$10$qqYLGDZ5tb4ZdkLQhh5zUeBv6cSplSVJcEpocSfXtXAWK1.pNjMEW', 0, 0, '0', 0, 0, 0),
	(58, 'test56', '$2y$10$elTVs4AnFDVFCCtJDqPVPeO5QFMtysIsSzz8NmXYsBn6dYCp8OdNW', 0, 0, '0', 0, 0, 0),
	(59, 'test57', '$2y$10$YTYqccUQQLR7y0eu0LF2puqlHMDAR5e3kc9jb0J9eV9GJ0SOOyfC6', 0, 0, '0', 0, 0, 0),
	(60, 'test58', '$2y$10$ypscbk5yypq5zFLqUSGXg.9SweyoFQd3M6sd25pb39p1q4HaNrmVK', 0, 0, '0', 0, 0, 0),
	(61, 'test59', '$2y$10$uBhTKMpI/kH.jIM2h6avV.VN6AQnHQh6jINvQRMVS7EAn713dWGM6', 0, 0, '0', 0, 0, 0),
	(62, 'test60', '$2y$10$cTqf8rSn15tEQlD7onfaXue5/o3Dq4/fw09jbw0wCx.tzgLAuISBi', 0, 0, '0', 0, 0, 0),
	(63, 'test61', '$2y$10$GsuIw0x3qtN/lkH.Xjno6uaeuiUGf8fMvoFIuZ9TdgmPHc7VY.VHq', 0, 0, '0', 0, 0, 0),
	(64, 'test62', '$2y$10$qdiwLXViCUgI/XGzLIpV0uj4wpzpqH0gj.m2J.zjO9wErrA4Ja1h2', 0, 0, '0', 0, 0, 0),
	(65, 'test63', '$2y$10$qcLVmDqBiCog774YNqIPBOyy1FR.8eHK6oQeRXFUD86YpOkBKG8LO', 0, 0, '0', 0, 0, 0),
	(66, 'test64', '$2y$10$gNxKvb31MmwHzOtKtDWWdO/2LLlbVemhTmhJ6e0.fP4tFZgkFLEQ6', 0, 0, '0', 0, 0, 0),
	(67, 'test65', '$2y$10$.RceWX0j1NK1eQ.m9SwFkOYPUmPDgHqFyKihsLNYS/XZIfZE2XPqW', 0, 0, '0', 0, 0, 0),
	(68, 'test66', '$2y$10$gGe4TpAHEyjsu8hblqD5Xu2y4OA51lbxrlYSIC2sb0pFb1jjKIUMK', 0, 0, '0', 0, 0, 0),
	(69, 'test67', '$2y$10$N/WpYHbScQFyzvl97JErCuz5k4qSVPj44LNmBzCn7Vno7q0a9oNNa', 0, 0, '0', 0, 0, 0),
	(70, 'test68', '$2y$10$ybGyEyK5Wqq1BwfwG7S86OjhzBtSyrcj2XSzl6Lk5OG4sBlO3J8Hm', 0, 0, '0', 0, 0, 0),
	(71, 'test69', '$2y$10$ydg5VpvYk1awWbsUU15/hehb.1DG.RV8Q4.uNrX0X2XussLDUkYn2', 0, 0, '0', 0, 0, 0),
	(72, 'test70', '$2y$10$HR/nV4ZFGZTWxQ5q4/KGGOCSPKC7tCJnt7AVPyqyp3/9VED2nipGu', 0, 0, '0', 0, 0, 0),
	(73, 'test71', '$2y$10$yTzbtcuTNlWOJm5LqlBGmO11sYo6nhM7D0o/lJ2ioNnIlqGYO.7lu', 0, 0, '0', 0, 0, 0),
	(74, 'test72', '$2y$10$Odzgl7/Vp6hOIw6SdU/hwu0RqS8KpW3IjIHGasLNAunNNGyfaoHk.', 0, 0, '0', 0, 0, 0),
	(75, 'test73', '$2y$10$EuYy03HnjeGleCmwMwF56O4RJ13Y6Le8TAs/5L6qamRQ9RHMvb3M.', 0, 0, '0', 0, 0, 0),
	(76, 'test74', '$2y$10$wp3HcSCK.j.sV/CIuEUzMulmUZ0CwE2UW9DYw0ARoC/S4H22iJXeq', 0, 0, '0', 0, 0, 0),
	(77, 'test75', '$2y$10$kI3DOlSZ.CGChBiHjAVyEu5sYQG8dlYcb2AqsQb1nrWyQ23xR.MBK', 0, 0, '0', 0, 0, 0),
	(78, 'test76', '$2y$10$d7aa5GbL8i1W6GK87gMobO2aIEHw2iCquwlgv/dsFfFKM8U0WPxgu', 0, 0, '0', 0, 0, 0),
	(79, 'test77', '$2y$10$Qtd8uN0MkHGEGAz40bwOJOtwx5BxFKew0IyUdLC4BuATtKJEXSan6', 0, 0, '0', 0, 0, 0),
	(80, 'test78', '$2y$10$fVGtRrRBI49Q6baAZAlqlOHucbGEskyVJcHxDpNx2wRsNmZsXFKI.', 0, 0, '0', 0, 0, 0),
	(81, 'test79', '$2y$10$MOBxENn7AIOOoUx8HrGMr.zCvF6tbFQBjhs879tUc/Q/ZOgIZeg1u', 0, 0, '0', 0, 0, 0),
	(82, 'test80', '$2y$10$7BvjS.NrgNGzh54Uw9wXEuQKeHBOl.4koL1NXVkhLXr/guvBfxwCy', 0, 0, '0', 0, 0, 0),
	(83, 'test81', '$2y$10$P7bs0DuAnbzikohpqoh4wuIcICpKnTpH92qPfzPIHbxw9TQC.iE8m', 0, 0, '0', 0, 0, 0),
	(84, 'test82', '$2y$10$apt9qzBHEug2KEJkUFCJPOCsUMO9GfZ4X05SlwfW9LIvZ0hdGsg3e', 0, 0, '0', 0, 0, 0),
	(85, 'test83', '$2y$10$zljuhbUFpkd4s2/zY8khxesaKWES.Qp6ju1bypH2BkVNtNFJzu3xi', 0, 0, '0', 0, 0, 0),
	(86, 'test84', '$2y$10$XDrItPPdDzgZvrzxxkHRquXH0A3P4qSjMxT5gWpWpM037ECviei4K', 0, 0, '0', 0, 0, 0),
	(87, 'test85', '$2y$10$PgFyFNOvrqQs5xhn5KnSDuO9r7Eu6yX/lAOwjO8Q.fag117dg5OSS', 0, 0, '0', 0, 0, 0),
	(88, 'test86', '$2y$10$EMaQqFNgCS65mo3xxrRt7OCOOGom1rMA.iCvG6ePB0Q6lPu7ByuwS', 0, 0, '0', 0, 0, 0),
	(89, 'test87', '$2y$10$2yT0TL966xBKY4FKXetpJOfqni1Csy/cVhqwZ7aLIZPbnbPg1nZtG', 0, 0, '0', 0, 0, 0),
	(90, 'test88', '$2y$10$9wLwCISes0LJJbGagrh6Ruth93RIJXZ4QXR5g/SS9e0m2rcYovPYu', 0, 0, '0', 0, 0, 0),
	(91, 'test89', '$2y$10$tmoCLIsbuUPLEAdWt6JbAeKQ5So7VfJOVtcyv.HYAlLof2aFav0qu', 0, 0, '0', 0, 0, 0),
	(92, 'test90', '$2y$10$Ik.bCyrDEt8TX4rcXcDnOeoR/x4gbOiwomy4MoGi0Kfk9UR2p4O3a', 0, 0, '0', 0, 0, 0),
	(93, 'test91', '$2y$10$kvXdEeP9gBysJFjJxGjS6OXDp1/qb3TOZrQuUEuErP5Prt23pBz6m', 0, 0, '0', 0, 0, 0),
	(94, 'test92', '$2y$10$rM.Sh2gEFQWF7FNLS1wUiuZGXNlL6cwUzSJDOmfoKzRwtGIHKmcbS', 0, 0, '0', 0, 0, 0),
	(95, 'test93', '$2y$10$9ers048sbAgwvgq9C7oVce.Pap3nCvqErds5Gi5BvDBAXdtfJzojC', 0, 0, '0', 0, 0, 0),
	(96, 'test94', '$2y$10$g92pALAWDGfj6LvtZ.1pEuDDWK/6cXNZfhkcDMNZRJ1BYC7F2jYoe', 0, 0, '0', 0, 0, 0),
	(97, 'test95', '$2y$10$FrJ3iAt7w.cD6Pxv9iH8Luo0EUlWhr5EG04Y3GgTJ1JnH0/4vokxW', 0, 0, '0', 0, 0, 0),
	(98, 'test96', '$2y$10$OmBv/JKTEmAeuWwBeunfF.UU4HOfBxCQbyhiqVPBZDxd12ODmZVwe', 0, 0, '0', 0, 0, 0),
	(99, 'test97', '$2y$10$VPWTGE8zUQblgT8QtOtw6.NMp9MTLluzeDWzrVOmWW/8PGlJGw1.y', 0, 0, '0', 0, 0, 0),
	(100, 'test98', '$2y$10$tRSvj1ORObMEjGhZOZcOZer3pgCAGLEz5g6EQkTT2z8RQ.tjGfnNK', 0, 0, '0', 0, 0, 0),
	(101, 'test99', '$2y$10$40bHR.VXFJ89m4mnZyeFV.o3o74Ow2RJzGrD3hXT5FaJgKyK.gDzK', 0, 0, '0', 0, 0, 0);
/*!40000 ALTER TABLE `yf_users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
