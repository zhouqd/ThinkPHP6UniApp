<?php
namespace app\admin\controller;

use think\facade\App;

class Index
{
    public function index()
    {
        //加载后台主页模板
        return view();
    }

    //后台欢迎页面
    public function welcome(){
        return view();
    }

}
